require("vis")

local bpts = {}

bpts.bullets = function()
	local win = vis.win
	local startloc = win.selection.pos; local startmode = vis.mode;
	local letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	vis:command(", x/^(\t)*([0-9]|[A-Z]|[a-z])\\. / x/[0-9]|[A-Z]|[a-z]/")
	local runs = {}; setmetatable(runs,{__index = function () return 0 end})
	local oldcol = 1
	for s in win:selections_iterator() do
		runs[s.col] = runs[s.col]+1
		if s.col < oldcol then for i = s.col+1,#runs do table.remove(runs) end end
		oldcol = s.col
		win.file:insert(s.pos,(s.col == 1) and runs[1] or (s.col == 2) and letters:sub(runs[2],runs[2]) or string.lower(letters:sub(runs[s.col],runs[s.col])))
		win.file:delete(s.pos,1)
	end
	win.selection.pos = startloc; vis:feedkeys("<vis-mode-normal-escape>") vis.mode = startmode;
end

bpts.indent = function()
	local win = vis.win
	local position = win.selection.pos+1
	win.file.lines[win.selection.line] = "	"..win.file.lines[win.selection.line]
	win.selection.pos = position
	bpts:bullets()
end

bpts.unindent = function()
	local win = vis.win
	if win.file.lines[win.selection.line]:sub(1,1) == "	" then
		local position = win.selection.pos-1
		win.file.lines[win.selection.line] = win.file.lines[win.selection.line]:sub(2)
		win.selection.pos = position
		bpts:bullets()
	end
end

bpts.init = function()
	vis:map(vis.modes.INSERT, "<Tab>", function() bpts:indent() end)
	vis:map(vis.modes.INSERT, "<S-Tab>", function() bpts:unindent() end)
	vis:map(vis.modes.INSERT, "<Enter>", function() vis:feedkeys("<vis-motion-line-end><vis-insert-newline>1. "); bpts:bullets() end)
	vis:map(vis.modes.INSERT, "<S-Enter>", "<vis-insert-newline>")
	vis:map(vis.modes.NORMAL, "<Tab>", function() bpts:bullets() end)
	vis.options.ai = true
end

return bpts
